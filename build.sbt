
mainClass in Compile := Some("app.Main")

lazy val root = (project in file("."))
  .aggregate(lib1, lib2)
  .dependsOn(lib1, lib2)

lazy val lib1 = (project in file("submodule-library1"))

lazy val lib2 = (project in file("submodule-library2"))
